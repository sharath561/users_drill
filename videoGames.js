//Q1 Find all users who are interested in playing video games.

//const users = require("./usersData");

//console.log(users)

function videoGame(data) {
  //console.log(data)

  if (typeof data === "object") {
    const usersData = [];

    for (const user in data) {
      //console.log(data[user].interests, 1);
      //console.log(data[user].interest, 2);

      if (data[user].interests) {
        //console.log(data[user].interests,"first")

        for (let joy in data[user].interests) {
          if (data[user].interests[joy].includes("Video Games")) {
            usersData.push(user);
            break;
          }
        }
      } else if (data[user].interest) {
        for (let inte in data[user].interest) {
          if (data[user].interest[inte].includes("Video Games")) {
            usersData.push(user);
            break;
          }
        }
      }
    }
    return usersData;
  } else {
    return [];
  }
}

//console.log(videoGame(users));
module.exports = videoGame;
