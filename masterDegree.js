//Q3 Find all users with masters Degree.

//const users = require("./usersData");

function mastersDegree(data) {
  if (typeof data === "object") {
    const userData = [];

    for (let user in data) {
      if (data[user].qualification == "Masters") {
        userData.push(user);
      }
    }
    return userData;
  } else {
    return [];
  }
}

//console.log(mastersDegree(users));

module.exports = mastersDegree;
