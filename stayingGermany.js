//Q2 Find all users staying in Germany.

//const users = require("./usersData");

function germany(data) {
  if (typeof data === "object") {
    stayingData = [];

    for (let user in data) {
      if (data[user].nationality == "Germany") {
        stayingData.push(user);
      }
    }
    return stayingData;
  } else {
    return [];
  }
}

//console.log(germany(users));

module.exports = germany;
