//Q4 Group users based on their Programming language mentioned in their designation.

//const users = require("./usersData");

const removalData = ["Senior", "Intern", "Developer", "-"];

function takeData(data) {
  if (typeof data === "object") {
    const programmingdata = {};

    for (let user in data) {
      const duty = data[user].desgination;

      let newData = "";

      for (let char of duty) {
        if (char != " ") {
          newData += char;
        } else if (char == " ") {
          if (!removalData.includes(newData)) {
            if (programmingdata[newData]) {
              programmingdata[newData].push(user);
            } else {
              programmingdata[newData] = [user];
            }

            newData = "";
            break;
          } else {
            newData = "";
          }
        }
      }

      //console.log(newData, user);

      if (newData.length > 0 && !removalData.includes(newData)) {
        if (programmingdata[newData]) {
          programmingdata[newData].push(user);
        } else {
          programmingdata[newData] = [user];
        }
      }
    }

    return programmingdata;
  } else {
    return [];
  }
}

//console.log(takeData(users));

module.exports = takeData;

/*function programmingGroups(data){

    const groupsData = {}

    for (let user in data){

        if (groupsData[data[user].desgination]){

            groupsData[data[user].desgination].push(user)
        }
        else{

            groupsData[data[user].desgination] = [user]
        }
    }

    return groupsData
}

console.log(programmingGroups(users))*/
